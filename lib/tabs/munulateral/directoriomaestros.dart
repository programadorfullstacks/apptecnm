import 'package:flutter/material.dart';

class DirectorioMaestros extends StatelessWidget {
  final List<City> _allCities = City.allCities();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: new Text("DIRECTORIO"),
        ),
        body: Container(
          child: new Padding(
              padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
              child: getHomePageBody(context)),
        ));
  }

  getHomePageBody(BuildContext context) {
    return ListView.builder(
      itemCount: _allCities.length,
      itemBuilder: _getItemUI,
      padding: EdgeInsets.all(0.0),
    );
  }

  Widget _getItemUI(BuildContext context, int index) {
    return new Card(
        child: new Column(
      children: <Widget>[
        new ListTile(
          leading: new Image.asset(
            "assets/" + _allCities[index].image,
            fit: BoxFit.cover,
            width: 100.0,
          ),
          title: new Text(
            _allCities[index].name,
            style: new TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
          ),
          subtitle: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(_allCities[index].country,
                    style: new TextStyle(
                        fontSize: 13.0, fontWeight: FontWeight.normal)),
                new Text('correo institucional: ${_allCities[index].correo}',
                    style: new TextStyle(
                        fontSize: 11.0, fontWeight: FontWeight.normal)),
              ]),
          onTap: () {},
        )
      ],
    ));
  }
}

class City {
  //--- Name Of City
  final String name;
  //-- image
  final String image;
  //--- population
  final String correo;
  //--- country
  final String country;

  City({this.name, this.country, this.correo, this.image});

  static List<City> allCities() {
    var lstOfCities = new List<City>();

    lstOfCities.add(new City(
        name: "M.C. JOSÉ JAVIER PERALTA COSGAYA",
        country: "DIRECTOR",
        correo: "dir_china@tecnm.mx",
        image: 'itchinalogo.png'));
    lstOfCities.add(new City(
        name: "DIANA LETICIA MEDINA VELÁZQUEZ",
        country: "Secretaria",
        correo: "dir01_china@tecnm.mx",
        image: "itchinalogo.png"));
    lstOfCities.add(new City(
        name: "ING. MARCO GABRIEL ROSADO ÁVILA",
        country: "Subdirector academico",
        correo: "acad_china@tecnm.mx",
        image: "itchinalogo.png"));
    lstOfCities.add(new City(
        name: "LIC. MIRSHA GABRIELA MAGAÑA CRUZ",
        country: "Gefa de depto. ciencias basicas",
        correo: "cbas_china@tecnm.mx",
        image: "itchinalogo.png"));
    lstOfCities.add(new City(
        name: "MONICA BEATRIZ LÓPEX HERNANDEZ",
        country: "Gefe de depto. ingenierías",
        correo: "ing_china@tecnm.mx",
        image: "itchinalogo.png"));
    lstOfCities.add(new City(
        name: "MTRA. ELVIRA MARÍA QUETZ AGUIRRE",
        country: "Gefa de depto. ciencia economica y administrativas",
        correo: "cead_china@tecnm.mx",
        image: "itchinalogo.png"));
    lstOfCities.add(new City(
        name: "SUSANA GABRIELA RODRÍGUEZ SANTOS",
        country: "Gefa de depto desarrollo academico",
        correo: "dda_china@tecnm.mx",
        image: "itchinalogo.png"));

    lstOfCities.add(new City(
        name: "IGNACIO IVÁN REYES GÓMEZ",
        country:
            "Gefe de divición de estudios profcionales/titulacion/cordinación de lenguas extrangeras",
        correo: "dep_china@tecnm.mx",
        image: "itchinalogo.png"));

    return lstOfCities;
  }
}
