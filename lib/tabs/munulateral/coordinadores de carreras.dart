import 'package:flutter/material.dart';

class Coordinacion extends StatelessWidget {
  final List<Lista> _allCities = Lista.allCities();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: new Text("DIRECTORIO"),
        ),
        body: Container(
          child: new Padding(
              padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
              child: getHomePageBody(context)),
        ));
  }

  getHomePageBody(BuildContext context) {
    return ListView.builder(
      itemCount: _allCities.length,
      itemBuilder: _getItemUI,
      padding: EdgeInsets.all(0.0),
    );
  }

  Widget _getItemUI(BuildContext context, int index) {
    return new Card(
        child: new Column(
      children: <Widget>[
        new ListTile(
          leading: new Image.asset(
            "assets/" + _allCities[index].image,
            fit: BoxFit.cover,
            width: 60.0,
            height: 100,
          ),
          title: new Text(
            _allCities[index].name,
            style: new TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          subtitle: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(_allCities[index].country,
                    style: new TextStyle(
                        fontSize: 13.0, fontWeight: FontWeight.normal)),
                new Text('correo institucional: ${_allCities[index].correo}',
                    style: new TextStyle(
                        fontSize: 11.0, fontWeight: FontWeight.normal)),
              ]),
          onTap: () {},
        )
      ],
    ));
  }
}

class Lista {
  //--- Name Of City
  final String name;
  //-- image
  final String image;
  //--- population
  final String correo;
  //--- country
  final String country;

  Lista({this.name, this.country, this.correo, this.image});

  static List<Lista> allCities() {
    var lstOfCities = new List<Lista>();

    lstOfCities.add(new Lista(
        name: "BRÍGIDO MANUEL LEE BORGES",
        country: "Ingenieria en agronomia",
        correo: "brigido.lb@china.tecnm.mx",
        image: 'cordinador1.png'));
    lstOfCities.add(new Lista(
        name: "JOSÉ GERMAIN LÓPEZ SANTIAGO",
        country: "Ingenieria forestal",
        correo: "jose.ls@china.tecnm.mx",
        image: "cordinador2.png"));
    lstOfCities.add(new Lista(
        name: "DELFINA MARGARITA CHAN UC",
        country: "Secretaria",
        correo: "delfina.cu@china.tecnm.mx",
        image: "cordinador3.png"));
    lstOfCities.add(new Lista(
        name: "GERARDO ALFONZO AVILÉS RAMIREZ",
        country: "Licenciatura en biologia",
        correo: "gerardo.ar@china.tecnm.mx",
        image: "cordinador4.png"));
    lstOfCities.add(new Lista(
        name: "ROGER ELIÉZER PÉREZ VELÁZQUEZ",
        country: "Ingenieria informatica",
        correo: "roger.pv@china.tecnm.mx",
        image: "cordinador5.png"));
    lstOfCities.add(new Lista(
        name: "DIDIER CERVANTES CRISANTY",
        country: "Ingenieria en administración",
        correo: "didier.cc@china.tecnm.mx",
        image: "didier.png"));
    lstOfCities.add(new Lista(
        name: "ALICIA EUGENIA PUERTOVANNETI ARROYO",
        country: "Ingenieria en gestión empresarial",
        correo: "alicia.pa@china.tecnm.mx",
        image: "cordinador6.png"));

    return lstOfCities;
  }
}
