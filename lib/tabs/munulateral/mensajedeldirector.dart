import 'package:flutter/material.dart';
import 'package:expandable_text/expandable_text.dart';

class Instituto extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: new Text("MENSAJE DEL DIRECTOR"),
        backgroundColor: Colors.blue[900],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            Container(
              child: Image.network(
                "https://scontent.fcjs3-1.fna.fbcdn.net/v/t1.6435-9/160046323_5224791484257495_7148519919830734531_n.jpg?_nc_cat=102&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeGggV9wFXOHYCDPpilWmVtHKtCM4kEvOCUq0IziQS84JTyxGem9xUQc83CpS3IamUALz8BPvFasNwhdQVpllnS4&_nc_ohc=7wgi1bRzxtsAX_E52Og&_nc_pt=1&_nc_ht=scontent.fcjs3-1.fna&oh=f1204ae46667b45edfae76cf8af51014&oe=60D302ED",
                fit: BoxFit.cover,
                width: 10,
              ),
              decoration: BoxDecoration(),
            ),
            ExpandableText(
              '',
              expandText: 'show more',
              collapseText: 'show less',
            ),
            SizedBox(height: 30.0),
            ExpandableText(
              'El Instituto Tecnológico de Chiná (ITChiná), a 45 años de su creación, ha formado a más de 2,680 profesionistas en las diferentes carreras que ha ofertado. Actualmente ofrece siete programas educativos: Ingeniería en Agronomía, Ingeniería Forestal, Ingeniería en Gestión Empresarial, Ingeniería en Administración, Licenciatura en Biología, Ingeniería Informática e Ingeniería en Industrias Alimentarias, así como las Maestrías en: Agroecosistemas Sostenibles (en el PNPC de CONACYT) y Administración.',
              expandText: '',
              collapseText: '',
              maxLines: 11,
              linkColor: Colors.blue,
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 30.0),
            ExpandableText(
              'El ITChiná inició a partir de abril de 2014 un proceso profundo de transformación, consistente en trabajar sobre el cumplimiento de indicadores de calidad. Para ello; se trabajó en una autoevaluación con fines de acreditación de todos sus programas académicos acreditables, logrando a la fecha la acreditación de todos.',
              expandText: 'Seguir leyendo',
              collapseText: 'regresar',
              maxLines: 2,
              linkColor: Colors.blue,
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 30.0),
            ExpandableText(
              'Para atender los servicios ofertados, el ITChiná cuenta con los siguientes recursos humanos: 66 profesores y 27 de apoyo y asistencia a la educación. Del total de profesores, 9 cuentan con doctorado y 36 con maestría; de los cuales 7 están en el Sistema Nacional de Investigadores (SNI) y 10 cuentan con el reconocimiento de Perfil Deseable del PROMEP. Actualmente se encuentran operando 3 cuerpos académicos: Investigación Agropecuaria y Forestal para el Desarrollo Sustentable del Trópico, Agroecología y Desarrollo Sustentable y Agroescosistemas y Conservación de la Biodiversidad.',
              expandText: 'Seguir leyendo',
              collapseText: 'cerrar',
              maxLines: 2,
              linkColor: Colors.blue,
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      ),
    );
  }
}
