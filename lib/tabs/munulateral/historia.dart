import 'package:flutter/material.dart';

class Historia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: new Text("HISTORIA"),
        ),
        body: ListView(
          children: [
            Container(
              padding: EdgeInsets.all(20.0),
              child: Image.network(
                  "http://tribunacampeche.com/wp-content/uploads/2015/10/INSTITUTO-TECNOLOGICO-DE-CHINA-8.jpg"),
            ),
            Column(
              children: [
                Text(
                  "Éste fue el primer tecnológico creado en el Estado de Campeche, el 1 de septiembre de 1975 abrió sus puertas a la juventud estudiosa con vocación agropecuaria. Siendo el quinto de su tipo a nivel nacional, se le denominó Instituto Tecnológico Agropecuario Núm. Cinco  (ITA 5), dependiendo de la Dirección General de Educación Tecnológica Agropecuaria (DGETA).",
                  textAlign: TextAlign.justify,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "En febrero de 2019, el M.C. José Javier Peralta Cosgaya es nombrado por el Dr. Enrique Fernández Fassnacht, Director General del TecNM, como nuevo Director, reiterando el compromiso con la comunidad tecnológica y con la sociedad de: “Aprender Produciendo”.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "",
                  textAlign: TextAlign.justify,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ));
  }
}
