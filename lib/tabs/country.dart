import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

void main() => runApp(MaterialApp(
      home: (Countrytabs()),
    ));

class Countrytabs extends StatefulWidget {
  @override
  _CountrytabsState createState() => _CountrytabsState();
}

class _CountrytabsState extends State<Countrytabs> {
  @override
  Widget build(BuildContext context) {
    // ignore: non_constant_identifier_names
    Widget image_slider_carousel = Container(
      width: 100,
      height: 200,
      padding: EdgeInsets.all(5.0),
      child: Carousel(
        boxFit: BoxFit.fill,
        images: [
          AssetImage('img/1.jpg'),
          AssetImage('img/2.jpg'),
          AssetImage('img/3.jpg'),
          AssetImage('img/4.jpg'),
        ],
        autoplay: true,
        indicatorBgPadding: 1.0,
        dotBgColor: Colors.blue[900],
        dotColor: Colors.green[500],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        // Title
        title: Text("CONTACTO",
            style: TextStyle(fontSize: 15, color: Colors.white)),
        // Set the background color of the App Bar
        backgroundColor: Colors.blue[900],
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(height: 10),
          nombre(),
          image_slider_carousel,
          ListTile(
            title: Text("Mensaje",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 35.0,
                    fontWeight: FontWeight.bold)),
            subtitle: RichText(
              textAlign: TextAlign.justify,
              text: TextSpan(
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic),
                  children: [
                    TextSpan(
                        text:
                            '"En estos tiempos dificiles, la tecnologia juega un papel tan importante que incluso es la unica manera para que el mundo no se detenga, la educacion no es una excepción. Es por eso que la tematica de esta App o más bien el objetivo, es el dar a conocer el Tecnologico De Chiná pero de una forma distinta en la cual los aspirantes y sociedad en general puedan dar un vistazo a las bastas areas que el instituto ofrece, todo esto pensando en las muchas dificultades de movilización que existen en nuestro país"'),
                  ]),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Column(
            children: [
              ListTile(
                title: Text('Medios de contacto'),
              ),
              ListTile(
                // leading: Icon(Icons.photo_album, color: Colors.blue),

                title: Text("Dirección"),

                subtitle: RichText(
                  textAlign: TextAlign.justify,
                  text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                        TextSpan(
                            text:
                                'Calle 11 S/N ENTRE 22 Y 28, CHINÁ, CAM. MÉXICO. C.P. 24520'),
                      ]),
                ),
              ),
              ListTile(
                // leading: Icon(Icons.photo_album, color: Colors.blue),
                title: Text("Correo Electronico y Numeros Telefonicos"),

                subtitle: RichText(
                  textAlign: TextAlign.justify,
                  text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                        TextSpan(
                            text:
                                'Email: dir01_china@tecnm.mxTeléfonos: (981) 82 7 20 81, 82 y 52 Ext. 101 y 103 '),
                      ]),
                ),
              ),
              ListTile(
                // leading: Icon(Icons.photo_album, color: Colors.blue),

                title: Text("Facebook"),

                subtitle: RichText(
                  textAlign: TextAlign.justify,
                  text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                        TextSpan(text: '@tecnmcampus.china'),
                      ]),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

Widget nombre() {
  return Text("ESTUDIATES GRADUADOS EN TECNM",
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold));
}
