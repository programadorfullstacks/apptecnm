import 'package:flutter/material.dart';

class Slide {
  final String imageUrl;
  final String title;
  final String description;

  Slide({
    @required this.imageUrl,
    @required this.title,
    @required this.description,
  });
}

final slideList = [
  Slide(
    imageUrl: 'assets/Fotos/AGRONOMIA.jpg',
    title: 'INGENIERÍA AGRONOMIA',
    description:
        'profesionales con capacidad sustentable de producción agropecuaria tecnologica con estandares de calidad.',
  ),
  Slide(
    imageUrl: 'assets/Fotos/forestal.jpg',
    title: 'INGENIERÍA FORESTAL',
    description:
        'Profecionales con conicmientos cientificos y tecnológicos de ecosistemas forestales sustentables.',
  ),
  Slide(
    imageUrl: 'assets/Fotos/industrial.jpg',
    title: 'INGENIERÍA EN INDUSTRIAS ALIMENTARIAS',
    description:
        'Profesionales con bases éticas, científicas, tecnológicas e innovadoras en sistemas de producción alimentaria.',
  ),
  Slide(
    imageUrl: 'assets/Fotos/biologia.jpg',
    title: 'LICENCIATURA EN BIOLOGIA',
    description:
        'profecionales competentes para el manejo y conservación de la biodiversidad y a la diversidad cultural.',
  ),
  Slide(
    imageUrl: 'assets/Fotos/informatica.jpg',
    title: 'INGENIERÍA INFORMATICA',
    description:
        'Profesionales conpetentes para ofrecer servicios informáticos y gestión de proyectos de acuerdo con las necesidades actuales.',
  ),
  Slide(
    imageUrl: 'assets/Fotos/administracion.jpg',
    title: 'INGENIERÍA EN ADMINISTRACIÓN',
    description:
        'Profecionales competentes en la dirección e innovación, emprendedores de un entorno globalizado.',
  ),
  Slide(
    imageUrl: 'assets/Fotos/empresarial.jpg',
    title: 'INGENIERÍA EN GESTION EMPRESARIAL',
    description:
        'Profesionales capaces de contribuir a la gestión, Innovación, diseño y desarrollo de procesos estratégicos empresariales y de negocios.',
  ),
  Slide(
    imageUrl: 'assets/Fotos/sostenible.jpg',
    title: 'MAESTRIA EN CIENCIAS EN AGROSISTEMA SOSTENIBLE',
    description: '',
  ),
  Slide(
    imageUrl: 'assets/Fotos/maestria.jpg',
    title: 'MAESTRIA EN ADMINISTRACIÓN',
    description: '',
  ),
];
